<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Medico;
use App\Clinica;
use App\Cita;

class CitasTableSeeder extends Seeder
{
    /**
     * Genera una fecha de forma aleatoria.
     * @param $start
     * @param $end
     * @param string $format
     * @return false|string
     */
    private function randomDateTime($start, $end, $format = 'Y-m-d H:i:s')
    {
        $start = strtotime($start);
        $end = strtotime($end);
        $rand = mt_rand($start, $end);
        return date($format, $rand);
    }


    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $medicos = Medico::pluck('id');
        $clinicas = Clinica::pluck('id');
        $fecha = today()->addDay()->format('Y-m-d');
        $min_cita = Carbon::parse($fecha . ' 09:00:00')->format('Y-m-d H:i:s');
        $max_cita = Carbon::parse($fecha . ' 18:00:00')->format('Y-m-d H:i:s');
        $dura_cita = 20;
        for ($i = 1; $i <= 100; $i++) {
            $id_clinica = $clinicas->random();
            $id_medico = $medicos->random();
            $hora_inicio = $this->randomDateTime($min_cita, $max_cita, 'Y-m-d H:i:00');
            $hora_fin = Carbon::parse($hora_inicio)->addMinutes($dura_cita)->format('Y-m-d H:i:00');
            $cita = Cita::where('id_medico', '=', $id_medico)
                ->where('id_clinica', '=', $id_clinica)
                ->where(function ($query) use ($hora_inicio, $hora_fin) {
                    $query->whereBetween('hora_inicio', [$hora_inicio, Carbon::parse($hora_fin)->subSecond()->format('Y-m-d H:i:s')])
                        ->orWhereBetween('hora_fin', [Carbon::parse($hora_inicio)->addSecond()->format('Y-m-d H:i:s'), Carbon::parse($hora_fin)->subSecond()->format('Y-m-d H:i:s')])
                        ->orWhere(function ($query2) use ($hora_inicio, $hora_fin) {
                            $query2->where('hora_inicio', '<=', $hora_inicio)->where('hora_fin', '>=', $hora_fin);
                        });
                })->first();
            if (is_null($cita)) {
                $cita = new Cita([
                    'hora_inicio' => $hora_inicio,
                    'hora_fin' => $hora_fin,
                    'duracion' => $dura_cita,
                    'id_clinica' => $id_clinica,
                    'id_medico' => $id_medico,
                    'reservada' => false
                ]);
                $cita->save();
            }
        }
    }
}
