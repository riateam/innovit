<?php

use Illuminate\Database\Seeder;
use App\User;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
                'email' => 'usuario1@test.org',
                'nombre' => 'Usuario Uno',
                'password' => bcrypt('123456')
            ],
            [
                'email' => 'usuario2@test.org',
                'nombre' => 'Usuario Dos',
                'password' => bcrypt('123456')
            ],
            [
                'email' => 'usuario3@test.org',
                'nombre' => 'Usuario Tres',
                'password' => bcrypt('123456')
            ]
        ];

        foreach ($usuarios as $usuario) {
            $user = User::where('email', '=', $usuario['email'])->first();
            if (is_null($user)) {
                $user = User::create($usuario);
                $user->save();
            }
        }
    }
}
