<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Ejecutar los Seeder de la aplicación.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ClinicasTableSeeder::class,
            MedicosTableSeeder::class,
            UsuariosTableSeeder::class,
            CitasTableSeeder::class
        ]);
    }
}
