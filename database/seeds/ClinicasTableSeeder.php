<?php

use Illuminate\Database\Seeder;
use App\Clinica;

class ClinicasTableSeeder extends Seeder
{
    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $clinicas = [
            'Clínica Santiago',
            'Clínica Internacional',
            'Clínica Americana'
        ];

        foreach ($clinicas as $clinica) {
            $cli = Clinica::where('nombre', '=', $clinica)->first();
            if (is_null($cli)) {
                $cli = Clinica::create([
                    'nombre' => $clinica
                ]);
                $cli->save();
            }
        }
    }
}
