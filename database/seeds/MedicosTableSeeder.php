<?php

use Illuminate\Database\Seeder;
use App\Medico;

class MedicosTableSeeder extends Seeder
{
    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $medicos = [
            'Gregory House',
            'Lisa Cuddy',
            'James Wilson',
            'Robert Chase',
            'Allison Cameron',
            'Eric Foreman'
        ];

        foreach ($medicos as $medico) {
            $med = Medico::where('nombre', '=', $medico)->first();
            if (is_null($med)) {
                $med = Medico::create([
                    'nombre' => $medico
                ]);
                $med->save();
            }
        }
    }
}
