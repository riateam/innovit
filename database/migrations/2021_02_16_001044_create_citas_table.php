<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Ejecutar la migración.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('hora_inicio');
            $table->dateTime('hora_fin');
            $table->unsignedBigInteger('duracion');
            $table->unsignedBigInteger('id_usuario')->nullable();
            $table->unsignedBigInteger('id_clinica');
            $table->unsignedBigInteger('id_medico');
            $table->boolean('reservada')->default(false);
            $table->timestamps();
            $table->foreign('id_usuario')->references('id')->on('usuarios');
            $table->foreign('id_clinica')->references('id')->on('clinicas');
            $table->foreign('id_medico')->references('id')->on('medicos');
        });
    }

    /**
     * Revertir la migración.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
