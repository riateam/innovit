# Prueba técnica

Hola estimad@.

Te damos la bienvenida a la prueba técnica de Innovit.

## Aspectos generales
- Dispones de 210 minutos (3 horas y 30 minutos) para desarrollar lo solicitado y realizar la entrega.
- Se evaluará el uso de Git, Laravel, Vue y MySQL.

## Instrucciones

1. Realizar un **fork** del repositorio.
2. Desarrollar lo solicitado (ver sección *Desarrollos solicitados*).
3. Responder a las preguntas finales (ver sección *Preguntas finales*).
4. Solicitar un Pull Request desde su repositorio a la rama master de nuestro repositorio (Esto será considerado como prueba finalizada).

## Proyecto

El proyecto consiste en un sistema de reservas de citas médicas, en el cual los usuarios pueden:

- Ingresar a su cuenta (con correo electrónico y contraseña)
- Reservar una cita médica.
- Ver sus citas médicas vigentes.

### Base de datos

Consiste en 4 tablas (excluyendo migrations).

### Tabla Clinicas

Almacena todas las clínicas, su estructura es:

- id
- nombre
- created_at
- updated_at

### Tabla medicos

Almacena todos los médicos, su estructura es:

- id
- nombre
- created_at
- updated_at

### Tabla usuarios

Almacena todos los usuarios sin distinción de roles, su estructura es:

- id
- email
- nombre
- password
- created_at
- updated_at

### Tabla citas

Almacena todas las citas médicas sin importar su condición, su estructura es:

- id
- hora_inicio (Cuando comienza la cita)
- hora_fin (Cuando finaliza la cita)
- duracion (Duración en minutos)
- id_usuario
- id_clinica
- id_medico
- reservada (Campo boolean indicando si una cita se encuentra disponible o reservada)
- created_at
- updated_at

## Desarrollos solicitados

Para poder realizar los desarrollos, usted deberá primero crear las tablas de la base de datos y poblarlas de datos.
Para esto usted deberá ejecutar el comando:

`
php artisan migrate --seed
`

Tras su ejecución, se crearán 3 clínicas, 3 médicos, entre 1 a 100 citas disponibles y 3 usuarios.

**Usuarios para probar**

| nombre      | email             | password |
|-------------|-------------------|----------|
| Usuario Uno | usuario1@test.org | 123456   |
| Usuario Dos | usuario2@test.org | 123456   |
| Usuario Tres | usuario3@test.org | 123456   |


### Citas vigentes (URL: `/admin/citas/historial`)

Usted deberá desarrollar la funcionalidad que permita a un usuario listar todas sus citas vigentes, entendiendo como cita vigente a toda cita que su hora de inicio se encuentre en una fecha posterior a la actual.

Para poder desarrollar dicha funcionalidad, usted deberá completar el método `historial` que se encuentra en el controlador `CitasController`. La vista final debe mostrar:

- fecha (Ejemplo `18-02-2021`)
- hora (Ejemplo `15:30`)
- duracion (Ejemplo `20`)
- clínica (Ejemplo `Clínica Santiago`)
- médico (Ejemplo `Gregory House`)

### Reservar cita (URL: `/admin/citas/nueva`)

Para esto usted deberá primero completar el método `nuevaCupos` que se encuentra en el controlador `CitasController`, dicho método recibe como parámetros `id_medico` y `id_clinica`. Usted deberá poder obtener y retornar todas las citas vigentes que cumplan con los parámetros solicitados. 
Con los datos obtenidos, deberá mostrarlos en una tabla dentro del componente `Nueva.vue`, dicha tabla debe mostrar:

- fecha (Ejemplo `18-02-2021`)
- hora (Ejemplo `15:30`)
- duracion (Ejemplo `20`)
- opción (botón que permita realizar la reserva de dicha cita)

Posteriormente deberá desarrollar la funcionalidad que permita a un usuario poder reservar la cita seleccionada, para aquello deberá completar el método `reservar` que se encuentra en el componente `Nueva.vue`, dicho método recibe como parámetro el id de la cita y ejecuta una transacción hacia el backend.

`Tip: La url de la transacción se encuentra en this.routeReservar y se debe utilizar con POST`

Finalmente, deberá completar el método `nuevaReservar` que se encuentra en el controlador `CitasController`, dicho método recibe como parámetro `id_cita` y reserva dicha cita para el usuario en cuestión.


## Preguntas finales

Producto de las bajas temperaturas durante el invierno, la oferta de citas es mucho menor a la demanda que los usuarios tienen por obtenerlas.
Por lo cual, muchos usuarios intentan reservar la misma cita prácticamente en simultáneo, lo que conlleva que los usuarios obtengan muchos errores debido a que las citas ya no están disponibles.  

Usted como líder del proyecto:

**¿Qué haría para que los usuarios dejen de ver las citas que ya no están disponibles?, para esto se asume que cuando el usuario obtuvo inicialmente las citas *si* estaban disponibles.**

**Sin importar cómo usted piensa resolver la pregunta anterior, se genera el caso que más de un usuario intenta reservar de forma simultánea la misma cita. ¿Qué haría para que sólo un usuario pueda realizar la reserva sin problemas y los demás obtengan un error?**

## Contacto

En caso de tener dudas durante la prueba, puede contactarse mediante:

- **Correo electrónico**: eduardo@innovit.cl
- **Whatsapp**: +56963530372



#Mucho éxito en la prueba!


