<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    /**
     * La tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'citas';

    /**
     * Los atributos que son asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        'hora_inicio',
        'hora_fin',
        'duracion',
        'id_usuario',
        'id_clinica',
        'id_medico',
        'reservada'
    ];

    /**
     * Los atributos que se deben convertir en tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        'reservada' => 'boolean',
    ];

    /**
     * Obtiene el usuario de la cita.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'id_usuario');
    }

    /**
     * Obtiene la clínica de la cita.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clinica()
    {
        return $this->belongsTo('App\Clinica', 'id_clinica');
    }

    /**
     * Obtiene el médico de la cita.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medico()
    {
        return $this->belongsTo('App\Medico', 'id_medico');
    }
}
