<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    /**
     * La tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'clinicas';

    /**
     * Los atributos que son asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];

    /**
     * Obtiene las citas de la clínica.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function citas()
    {
        return $this->hasMany('App\Cita', 'id_clinica');
    }
}
