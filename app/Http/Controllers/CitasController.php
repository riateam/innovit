<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Clinica;
use App\Medico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CitasController extends Controller
{
    /**
     * Obtiene todas las citas vigentes del usuario
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historial()
    {
        $routeData = route('citas.obtener.citas');
        return view('citas.historial', compact('routeData'));
    }

    /**
     * Obtiene las citas
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function obtenerCitas(Request $request)
    {
        $citas = Cita::orderBy('hora_inicio', 'ASC')->with('medico')->with('clinica')->whereDate('hora_inicio', '>', now())->whereIdUsuario(auth()->user()->id)->get();
        return response()->json([
            'citas' => $citas
        ]);
    }

    /**
     * Vista para reservar una nueva cita.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function nueva()
    {
        $routeData = route('citas.nueva.data');
        $routeCupos = route('citas.nueva.cupos');
        $routeReservar = route('citas.nueva.reservar');
        return view('citas.nueva', compact('routeData', 'routeCupos', 'routeReservar'));
    }


    /**
     * Obtiene los médicos y clínicas del sistema.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function nuevaData(Request $request)
    {
        $medicos = Medico::orderBy('nombre', 'ASC')->select('id', 'nombre')->get();
        $clincias = Clinica::orderBy('nombre', 'ASC')->select('id', 'nombre')->get();
        return response()->json([
            'medicos' => $medicos,
            'clinicas' => $clincias
        ]);
    }

    /**
     * Obtiene todos los cupos disponibles para un médico y clínica.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function nuevaCupos(Request $request)
    {
        $cupos = Cita::orderBy('hora_inicio', 'ASC')->select('*')
            ->whereIdMedico($request->id_medico)
            ->whereIdClinica($request->id_clinica)
            ->whereReservada(false)->get();

        return response()->json([
            'cupos' => $cupos
        ]);
    }

    /**
     * Reserva una cita para el usuario.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function nuevaReservar(Request $request)
    {
        $cita = Cita::find($request->id_cita);
        $cita->id_usuario = auth()->user()->id;
        $cita->reservada = true;
        $cita->save();

        return response()->json([
            'cita' => $cita
        ]);
    }
}
