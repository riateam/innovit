<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    /**
     * La tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'medicos';

    /**
     * Los atributos que son asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];

    /**
     * Obtiene las citas del médico.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function citas()
    {
        return $this->hasMany('App\Cita', 'id_medico');
    }
}
