@extends('layouts.app')
@section('title','Mis citas')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Mis citas vigentes</div>

                    <div class="card-body">
                        <citas-vigentes route-data="{!! $routeData !!}"></citas-vigentes>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
