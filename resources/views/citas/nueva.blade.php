@extends('layouts.app')
@section('title','Reservar una cita')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Reservar una cita</div>

                    <div class="card-body" id="app">
                        <nueva-cita
                            route-data="{!! $routeData !!}"
                            route-cupos="{!! $routeCupos !!}"
                            route-reservar="{!! $routeReservar !!}"
                        ></nueva-cita>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
