@extends('layouts.app')
@section('title','Inicio')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    <p>
                        Hola estimad@.<br><br>
                        Te damos la bienvenida a la prueba técnica de Innovit.<br><br>
                        <strong>¡Te deseamos el mejor de los éxitos!</strong>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
