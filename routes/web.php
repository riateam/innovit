<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@indexPublico')->name('index.publico');

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    // Inicio
    Route::get('/', 'HomeController@indexPrivado')->name('index.privado');
    // Citas
    Route::group(['prefix' => 'citas'], function (){
        // Historial de citas
        Route::get('historial','CitasController@historial')->name('citas.historial');
        // Reservar una cita
        Route::get('nueva','CitasController@nueva')->name('citas.nueva');
        Route::post('nueva/data', 'CitasController@nuevaData')->name('citas.nueva.data');
        Route::post('nueva/cupos', 'CitasController@nuevaCupos')->name('citas.nueva.cupos');
        Route::post('nueva/reservar', 'CitasController@nuevaReservar')->name('citas.nueva.reservar');
        Route::get('listar','CitasController@obtenerCitas')->name('citas.obtener.citas');
    });
});
